from coreutils import shell
from coreutils import file_utils

def language_flag(track, language):
    return '--language %d:%s' % (int(track), language)

def create_language_flags(language_files):
    flags = []
    for track_id, language, filename in language_files:
        flags.append(language_flag(track_id, language))
        flags.append(shell.quote(filename))
    return ' '.join(flags)

def merge(output_file, video_filename, language_files, logfile):
    """ Merge digital video, audio and subtitles into a matroska file 
    format """
    langs = [(0, language, filename) for language, filename in language_files]

    command = ' '.join([
        'mkvmerge',
        "-q",
        "-o %s" % shell.quote(output_file),
        shell.quote(video_filename),
        create_language_flags(langs), 
    ])
    shell.execute(command, logfile)

def convert_to_mkv(output_filename, video_filename, subtitle, logfile):
    """ Restructure an digital video into a matroska file format """
    subs = []
    if subtitle:
        subs = [ (0, 'en', subtitle), ]

    if file_utils.extension(video_filename) == 'mp4':
        video_track = 1
        audio_track = 2
    else:
        video_track = 0
        audio_track = 1

    command = ' '.join([
        'mkvmerge',
        "-q",
        '-o %s' % shell.quote(output_filename),
        '--language %d:en' % audio_track,
        '--language %d:en' % video_track,
        shell.quote(video_filename),
        create_language_flags(subs),
    ])
    return shell.execute(command, logfile)

