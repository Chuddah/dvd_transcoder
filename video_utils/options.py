import os

from coreutils import options_file

class Options(object):

    def __init__(self, options_path):
        self.options_path = options_path
        self.load_options()

    def load_options(self):
        parser = options_file.parse_simple_options
        options = options_file.parse_file(self.options_path, parser)

        for option, value in options.iteritems():
            setattr(self, option, value)

options_path = os.path.expanduser('~/.dvd_encoder_options')
options = Options(options_path)
