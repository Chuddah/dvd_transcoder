import os

from coreutils import shell
from coreutils import re_utils
from coreutils import file_utils

from options import options
from language_standards import iso6391code

def m_command_base(m_command, dvd_path, title):
    return ' '.join([
        m_command,
        "-quiet",
        "dvd://%s" % title,
        "-dvd-device \"%s\"" % dvd_path,
    ])

def mplayer_identify_track(dvd_path, title, logfile):
    command = ' '.join([
        m_command_base(options.mplayer, dvd_path, title),
        "-identify",
        "-vo /dev/null",
        "-ao /dev/null"
    ])
    lines = shell.execute(command, logfile)
    return lines

audio_pattern = r"audio stream: (?P<stream>\d+) format: (?P<format>.*) language: (?P<lang>\w*) aid: (?P<id>\d+)"
subtitle_pattern = r"^subtitle \( sid \): (?P<id>\d+) language: (?P<lang>\w+)"
titles_pattern = r"ID_DVD_TITLE_(?P<title>\d+)_LENGTH=(?P<length>[\d\.]+)"
chapters_pattern = r"CHAPTERS: (?P<chapter_indicies>.*)"

hour = 60 * 60

def get_title_length(dvd_path, title_index, logfile):
    dvd_details = mplayer_identify_track(dvd_path, '01', logfile)
    title_lengths = dict([(int(title['title']), float(title['length'])) for title in re_utils.iterate_matches(dvd_details, titles_pattern)])
    return title_lengths[title_index]

def get_all_titles(dvd_path, logfile):
    dvd_details = mplayer_identify_track(dvd_path, '01', logfile)
    title_lengths = [(int(title['title']), float(title['length'])) for title in re_utils.iterate_matches(dvd_details, titles_pattern)]
    #chapter_indicies = [(int(title['title']), float(title['length'])) for title in re_utils.iterate_matches(dvd_details, chapters_pattern)]
    title_lengths.sort()

    if not title_lengths:
        logfile.write('No titles found, Exiting!\n')
        raise ValueError('No titles found')

    previous_lengths = set()
    result = []

    for index, title_length in title_lengths:
        if title_length not in previous_lengths and title_length > 9:
            result.append(('%02d' % index, title_length))
            previous_lengths.add(title_length)
    return result

# Video title analysis ###########################

def get_main_features(dvd_path, logfile):
    return [(title, length) for title, length in get_all_titles(dvd_path, logfile) if length > hour]

def get_main_feature(dvd_path, logfile):
    lengths = [(length, title) for title, length in get_all_titles(dvd_path, logfile)]
    if not lengths:
        return []
    lengths.sort()
    title = lengths[-1]
    return [(title[1], title[0])]

def get_series_titles(dvd_path, logfile):
    expected_length = 21
    error_delta = 3
    min_length = (expected_length - error_delta) * 60
    max_length = (expected_length + error_delta) * 60
    min_length = 20 * 60
    max_length = 55 * 60

    titles = [(title, length) for title, length in get_all_titles(dvd_path, logfile) if min_length <= length <= max_length]
    return titles

#####################################################

# Language title analysis ###########################

class AllLanguages(object):
    def is_desired_lang(self, lang):
        return True

class SelectedLanguages(object):
    def __init__(self, *desired_languages):
        self.desired_languages = desired_languages
    def is_desired_lang(self, lang):
        return lang in self.desired_languages

def _extract_language_details(dvd_details, pattern, language_picker):
    languages = list(re_utils.iterate_matches(dvd_details, pattern))
    languages = [lang for lang in languages if language_picker.is_desired_lang(lang['lang'])]

    for language in languages:
        lang = language['lang']  or 'Unknown'
        language['lang'] = lang
        language['language_str'] = iso6391code.get(lang, lang)
    return languages

def get_languages(dvd_path, title, logfile, language_picker=SelectedLanguages('', 'en', 'it')):
    dvd_details = mplayer_identify_track(dvd_path, title, logfile)
    subtitles = _extract_language_details(dvd_details, subtitle_pattern, language_picker)
    audios    = _extract_language_details(dvd_details, audio_pattern, language_picker)
    return audios, subtitles

def _get_languages(dvd_path, title, logfile, language_picker=SelectedLanguages('', 'en', 'it')):
    dvd_details = mplayer_identify_track(dvd_path, title, logfile)
    subtitles = _extract_language_details(dvd_details, subtitle_pattern, language_picker)
    audios    = _extract_language_details(dvd_details, audio_pattern, language_picker)
    #chapters = list(re_utils.iterate_matches(dvd_details, chapters_pattern)) or [dict(chapter_indicies='None')]
    #chapter_indicies = chapters[0]['chapter_indicies'].split(',')
    chapter_indicies = []
    return audios, subtitles, chapter_indicies


#####################################################

FILM, MUSIC, SERIES = range(3)

class SkipException(Exception):
    """ Execption raised when a directory is to be skipped """

def search_for_video_type(search_directory):
    try:
        file_items = list(file_utils.files(search_directory))
    except:
        raise SkipException('Error while searching %s' % search_directory)


    if '__skip__' in file_items:
        raise SkipException('__skip__ found in %s' % search_directory)

    if '__music__' in file_items:
        return MUSIC
    if '__series__' in file_items:
        return SERIES
    if '__film__' in file_items:
        return FILM

class OnlyEncode(object):
    def __init__(self, valid_video_types):
        self.valid_video_types = valid_video_types

    def __call__(self, search_directory):
        video_type = search_for_video_type(search_directory)
        if video_type not in self.valid_video_types:
            raise SkipException('Filtering out %s at users request' % search_directory)
        return video_type

def make_name_from_found_directory(found_directory):
    name = found_directory
    name = name.replace('_', ' ')
    name = name.replace(os.path.sep, ' - ')
    name = name.title()
    name = name.strip()
    #name = name.replace(' ', '_') # Hmm, remove once paths with spaces are dealt with
    return name

def search_for_dvds(search_directory, video_type=FILM, search_directory_root=None):
    search_directory_root = search_directory_root or search_directory
    try:
        video_type = search_for_video_type(search_directory) or video_type
        if 'VIDEO_TS' in file_utils.directories(search_directory):
            relative_found_directory = os.path.relpath(search_directory, search_directory_root)
            name = make_name_from_found_directory(relative_found_directory)
            path = os.path.join(search_directory, 'VIDEO_TS/')
            yield name, path, video_type

        for dir_item in file_utils.directories(search_directory):
            subdirectory = os.path.join(search_directory, dir_item)
            for result in search_for_dvds(subdirectory, video_type, search_directory_root):
                yield result
    except SkipException:
        pass # Keep going
