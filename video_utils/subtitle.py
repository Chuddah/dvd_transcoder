from coreutils import shell
import os
from options import options

#### LANGUAGE UTILS ##########################

def generator_to_list(fn):
    def generator_to_list_wrapper(*args, **kargs):
        return list(fn(*args, **kargs))
    return generator_to_list_wrapper 

###############################################


temp_directory = options.temp_directory

def m_command_base(m_command, dvd_path, title):
    return ' '.join([
        m_command,
        "-quiet",
        "dvd://%s" % title,
        "-dvd-device \"%s\"" % dvd_path,
    ])



def create_filename(name, language, title):
    filename = name + '.title%s.%s' % (title, language['lang'])
    return os.path.join(temp_directory, filename)

def rip_subtitle_track(dvd_path, filename, title, language, logfile):
    logfile.write('Extracting %s subtitle\n' % (language['language_str']))

    command = ' '.join([
        m_command_base(options.mencoder, dvd_path, title),
        "-sid %s" % language['id'],
        "-vobsubout %s" % shell.quote(filename),
        "-nosound",
        "-ovc raw -o /dev/null",
    ])
    shell.execute(command, logfile)

@generator_to_list
def rip_tracks(dvd_path, name, title, languages, logfile):
    previous_languages = set()
    #extras = []
    #subtitles = []
    for language in languages:
        lang = language['lang']
        if lang in previous_languages:
            continue
        filename = create_filename(name, language, title)
        rip_subtitle_track(dvd_path, filename, title, language, logfile)
        yield lang, '%s.%s' % (filename, 'idx')
        previous_languages.add(lang)
        #subtitles.append(extension(filename, 'sub'))
        #extras.append(extension(filename, 'idx'))
    #return subtitles, extras

if __name__ == '__main__':
    import dvd_analysis
    import sys
    logfile = sys.stdout

    dvd_path = '/home/user/videos/dvdrip/RED/'
    name = 'Red'
    title = 10
    _, languages = dvd_analysis.get_languages(dvd_path, title, logfile)

    print rip_subtitle_tracks(dvd_path, name, title, languages, logfile)

    """
    # LPCM (stereo)
    dvd_path = '/home/user/videos/dvdrip/RED/'
    output_file = '/home/user/red.lpcm'
    language = dict(id='0', lang='en', format='LPCM (stereo)')
    extract_lpcm_as_mp3(dvd_path, 1, language, output_file, logfile)

    #exit(0)

    # MPEG1 (stereo)
    dvd_path = '/home/user/videos/dvdrip/RED/'
    output_file = '/home/user/red.mpeg1.foo'
    language = dict(id='0', lang='en', format='MPEG1 (stereo)')
    extract_mpeg1_as_mp3(dvd_path, 8, language, output_file, logfile)
    exit(0)

    # AC3 (5.1)
    dvd_path = '/home/user/videos/dvdrip/RED/'
    output_file = '/home/user/red.ac3'
    language = dict(id='128', lang='en', format='AC3 (5.1)')
    extract_ac3(dvd_path, 10, language, output_file, logfile)

    exit(0)

    # AC3 (stereo)
    dvd_path = '/home/user/videos/dvdrip/RED/'
    output_file = '/home/user/red.ac3'

    language = dict(stream='0', lang='en', format='AC3 (stereo)')
    extract_ac3_as_mp3(dvd_path, 7, language, output_file, logfile)
    """
