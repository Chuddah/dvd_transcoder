from coreutils import shell
import os
import dvd_analysis
from options import options

temp_directory = options.temp_directory

def base_encoding_command(dvd_path, title, output_file):
    encoder_options = "ref=3:mixed-refs:bframes=6:b-pyramid=1:bime=1:b-rdo=1:weightb=1:analyse=all:8x8dct=1:subme=6:me=umh:merange=24:filter=-2-2:ref=6:mixed-refs=1:trellis=1:no-fast-pskip=1:no-dct-decimate=1:direct=auto"
    encoder = "x264"
    command = ' '.join([
        options.handbrake,
        "--decomb",
        "--deinterlace",
        #"--detelecine",
        "--deblock",
        "--two-pass",
        "--turbo",
        "--encoder %s" % encoder,
        "--%s %s" % (options.encopts, encoder_options),
        "--markers",
        "--audio none",
        "--input %s" % shell.quote(dvd_path),
        "--title %s" % title,
        "--output %s" % shell.quote(output_file),
        #"--quality %s" % 0.66,
        "--vb %s" % 1600,
    ])
    return command

def encode_sample(dvd_path, name, title, logfile, start=480, duration=60):
    logfile.write('Encoding sample video\n')

    output_file = '%s.title%s.mkv' % (name + '.sample', title)
    output_file = os.path.join(temp_directory, output_file)

    command = base_encoding_command(dvd_path, title, output_file)

    command = ' '.join([
        command,
        "--start-at duration:%d" % start,
        "--stop-at duration:%d" % duration,
    ])
    out = shell.execute(command, logfile)
    return output_file

def encode(dvd_path, name, title, logfile):
    logfile.write('Encoding video\n')

    output_file = '%s.title%s.mkv' % (name, title)
    output_file = os.path.join(temp_directory, output_file)

    command = base_encoding_command(dvd_path, title, output_file)

    title_length = int(dvd_analysis.get_title_length(dvd_path, int(title), logfile))
    command = ' '.join([
        command,
        "--stop-at duration:%d" % (title_length - 1),
    ])

    out = shell.execute(command, logfile)
    return output_file

if __name__ == '__main__':
    import sys
    logfile = sys.stdout
    dvd_path = '/home/user/videos/dvdrip/TRAINSPOTTING_1'
    name = 'sample_test.mkv'
    title = 01
    encode_sample(dvd_path, name, title, logfile)



