import os
import sys
from coreutils import shell
from coreutils.re_utils import iterate_matches
from options import options

def generator_to_list(fn):
    """ Decorator to convert generator functions to functions
    with a list of return values """
    def generator_to_list_wrapper(*args, **kargs):
        return list(fn(*args, **kargs))
    return generator_to_list_wrapper 


def get_scale(input_location, title, chapter, angle, audio_track, audio_type):
    '-L', # Loops through all the chapters,

    command = ' '.join([
        'tccat',
        '-T %s,%s,%s' % (title, chapter, angle),
        '-i %s' % input_location,
        '-t dvd',
        '-a %s' % audio_track,
        '-d 2', # Increases the verbosity
        ' | ', # pipe
        'tcextract',
        '-t vob',
        '-x %s' % audio_type,
        ' | ', # pipe
        'tcscan',
        '-x %s' % audio_type,
        '-d 1' # Verbosity, (I think)
    ])
    lines = shell.execute(command, sys.stderr)

    pattern = 'suggested volume rescale=(?P<rescale>[\d\.]+)'
    for match in iterate_matches(lines, pattern):
        return match['rescale']
    return '1.00'

def convert_audio(dvd_path, title, output_file, scale, audio_track, logfile):
    audio_bitrate=192
    command = ' '.join([
        'transcode',
        '--input %s' % dvd_path,
        '--import_with null,auto',
        '--export_with null,raw', # No video, or something like that (optimization)
        '--no_split',
        '--title %s,-1' % title,
        '--audio_scale %s' % scale,
        '--extract_track %s' % audio_track,
        '--abitrate %s' % audio_bitrate,
        '--audio_output %s' % output_file,
    ])
    return shell.execute(command, logfile)

def m_command_base(m_command, dvd_path, title):
    return ' '.join([
        m_command,
        "-quiet",
        "dvd://%s" % title,
        "-dvd-device \"%s\"" % dvd_path,
    ])

def extract_audio(dvd_path, title, output_file, audio_id, logfile):
    command = ' '.join([
        m_command_base(options.mplayer, dvd_path, title),
        "-aid %s" % audio_id,
        "-dumpaudio", 
        "-dumpfile \"%s\"" % output_file,
        "-vo /dev/null"
    ])
    shell.execute(command, logfile)
    return output_file


def create_filename(name, language, title, format):
    filename = '%s.title%s.%s.%s' % (name, title, language['lang'], format)
    return os.path.join(options.temp_directory, filename)

def _extract_as_mp3(dvd_path, input_audio_type, title, language, output_file, logfile):
    #raise ValueError('Cannot convert to mp3')
    audio_id = language['stream']
    chapter, angle = -1,1
    filename = create_filename(output_file, language, title, 'mp3')
    scale = get_scale(dvd_path, title, chapter, angle, audio_id, input_audio_type)

    return convert_audio(dvd_path, title, filename, scale, audio_id, logfile)
    return filename



def _convert_ac3_to_wav(ac3_file, wav_filename):
    command = ' '.join([
        'ffmpeg',
        "-i %s" % ac3_file,
        wav_filename,
    ])
    shell.execute(command, logfile)

def _convert_wav_to_mp3(wav_filename, mp3_filename, bitrate=224):
    command = ' '.join([
        'lame',
        "-h",
        "-b %s" % bitrate,
        wav_filename,
        mp3_filename,
    ])
    shell.execute(command, logfile)

def convert_ac3_to_wav(ac3_file, output_file, language, title):
    wav_filename = create_filename(output_file, language, title, 'wav')
    _convert_ac3_to_wav(ac3_file, wav_filename)
    return wav_filename

def convert_wav_to_mp3(wav_file, output_file, language, title):
    mp3_filename = create_filename(output_file, language, title, 'mp3')
    _convert_wav_to_mp3(wav_file, mp3_filename)
    return mp3_filename



def extract_ac3_as_mp3(dvd_path, title, language, output_file, logfile):
    ac3_filename = extract_ac3(dvd_path, title, language, output_file, logfile)
    wav_filename = convert_ac3_to_wav(ac3_filename, output_file, language, title)
    os.unlink(ac3_filename)
    mp3_filename = convert_wav_to_mp3(wav_filename, output_file, language, title)
    os.unlink(wav_filename)
    return mp3_filename

def extract_lpcm_as_mp3(dvd_path, title, language, output_file, logfile):
    return _extract_as_mp3(dvd_path, 'pcm', title, language, output_file, logfile)

def extract_mpeg1_as_mp3(dvd_path, title, language, output_file, logfile):
    return _extract_as_mp3(dvd_path, 'pcm', title, language, output_file, logfile)

def extract_ac3(dvd_path, title, language, output_file, logfile):
    audio_id = language['id']
    filename = create_filename(output_file, language, title, 'ac3')
    extract_audio(dvd_path, title, filename, audio_id, logfile)
    return filename



def priority_sorter(language):
    encoding_priority = [
        'ac3 (5.1)',
        'ac3 (5.1/6.1)',
        'dts (5.1/6.1)',
        'dts (5.1)',
        'ac3 (stereo)',
        'mpeg1 (stereo)',
        'lpcm (stereo)',
        'ac3 (mono)',
    ]
    return encoding_priority.index(language['format'])

def pick_best_audio_quality_per_language(languages):
    language_map = {}
    for language in languages:
        group = language_map.setdefault(language['lang'], [])
        group.append(language)

    chosen_tracks = []
    for group in language_map.itervalues():
        group.sort(key=priority_sorter)
        chosen_tracks.append(group[0])

    return chosen_tracks



def rip_audio(dvd_path, name, title, language, logfile):
    format = language['format']
    converter = {
        'lpcm (stereo)':   extract_lpcm_as_mp3,
        #'dts (5.1/6.1)':
        #'dts (5.1)':
        'ac3 (mono)':      extract_ac3_as_mp3,
        #'ac3 (stereo)':    extract_ac3_as_mp3,
        'ac3 (stereo)':    extract_ac3,
        'ac3 (5.1)':       extract_ac3,
        'ac3 (5.1/6.1)':   extract_ac3,
        'mpeg1 (stereo)':  extract_mpeg1_as_mp3,
    }[format]

    return converter(dvd_path, title, language, name, logfile)

@generator_to_list
def rip_tracks(dvd_path, name, title, languages, logfile):
    for language in pick_best_audio_quality_per_language(languages):
        yield language['lang'], rip_audio(dvd_path, name, title, language, logfile)




if __name__ == '__main__':
    languages = [
        dict(lang='en', format='ac3 (5.1)'),
        dict(lang='en', format='ac3 (stereo)'),
        dict(lang='it', format='ac3 (stereo)'),
        dict(lang='jap', format='dts (5.1)'),
        dict(lang='jap', format='dts (5.1/6.1)'),
    ]
    print pick_best_audio_quality_per_language(languages)
    print

    #exit(0)

    import dvd_analysis
    import sys
    logfile = sys.stdout

    """
    dvd_path = '/home/user/videos/dvdrip/RED/'
    name = 'Red'
    title = 10
    languages, _ = dvd_analysis.get_languages(dvd_path, title, logfile)

    rip_audio_tracks(dvd_path, name, title, languages, logfile)

    # LPCM (stereo)
    dvd_path = '/home/user/videos/dvdrip/RED/'
    output_file = '/home/user/red.lpcm'
    language = dict(id='0', lang='en', format='LPCM (stereo)')
    extract_lpcm_as_mp3(dvd_path, 1, language, output_file, logfile)

    #exit(0)

    # MPEG1 (stereo)
    dvd_path = '/home/user/videos/dvdrip/RED/'
    output_file = '/home/user/red.mpeg1.foo'
    language = dict(id='0', lang='en', format='MPEG1 (stereo)')
    extract_mpeg1_as_mp3(dvd_path, 8, language, output_file, logfile)
    exit(0)

    # AC3 (5.1)
    dvd_path = '/home/user/videos/dvdrip/RED/'
    output_file = '/home/user/red.ac3'
    language = dict(id='128', lang='en', format='AC3 (5.1)')
    extract_ac3(dvd_path, 10, language, output_file, logfile)

    exit(0)
    """

    # AC3 (stereo)
    dvd_path = '/home/user/videos/dvdrip/RED/'
    title = 7
    output_file = '/home/user/red.ac3'

    dvd_path = '/home/user/videos/dvdrip/THE_INBETWEENERS/SEASON3'
    title = 4
    output_file = '/home/user/inbetweners.season3.track4.ac3'

    language = dict(stream='0', lang='en', format='ac3 (stereo)', id='128')
    print extract_ac3_as_mp3(dvd_path, title, language, output_file, logfile)
    exit(0)

    language = dict(stream='1', lang='en', format='ac3 (stereo)')
    print extract_ac3_as_mp3(dvd_path, title, language, output_file, logfile)
    language = dict(stream='2', lang='en', format='ac3 (stereo)')
    print extract_ac3_as_mp3(dvd_path, title, language, output_file, logfile)


#print get_scale()
