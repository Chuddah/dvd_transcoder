from coreutils import shell
from coreutils.re_utils import iterate_matches
from options import options

def get_language_codes():
    language_codes = shell.execute('%s --list-languages' % options.mkvmerge)
    pattern = '(?P<language>[^\|]+)\| (?P<iso6392code>[^\|]+)[ ]*\|(?P<iso6391code>[^\|]*)'

    for match in iterate_matches(language_codes, pattern):
        yield (match['language'].strip(), match['iso6391code'].strip(), match['iso6392code'].strip())

iso6391code = dict((code, language) for language, code, _ in get_language_codes() if code)
iso6392code = dict((code, language) for language, _, code in get_language_codes() if code)

if __name__ == '__main__':
    for row in get_language_codes():
        print '%-68s%-06s%-06s' % row
