import re

from coreutils import file_utils
from coreutils import time_utils
import dvd_analysis
from options import options

title_line_format = '%(title)s "%(dvd_path)s" "%(name)s"'
title_line_parse = '(?P<title>\d+) "(?P<dvd_path>.+)" "(?P<name>.+)"'
comment_parse = '(?P<interesting>.*?)\s*#(?P<comment>.*)'

def read_encode_list(filename):
    comment_pattern = re.compile(comment_parse)
    encode_title = re.compile(title_line_parse)

    titles = []
    contents = file_utils.read_file(filename)
    for line in contents:
        comment = ''
        comment_match = comment_pattern.match(line)
        if comment_match:
            comment = comment_match.groupdict()['comment']
            line = comment_match.groupdict()['interesting']
        match = encode_title.match(line)

        if match:
            result = match.groupdict()
            result['comment'] = comment
            titles.append(result)
        else:
            pass

    return titles

def write_encode_list(filename, titles):
    lines = []
    for title in titles:
        if isinstance(title, str):
            lines.append(title)
        else:
            line = title_line_format % title
            if title['comment']:
                line = '%s #%s' % (line, title['comment'])
            lines.append(line)

    file_utils.write_file(filename, lines)

def read_to_do_encode_list():
    return read_encode_list(options.todo_list)

def write_to_do_encode_list(titles):
    write_encode_list(options.todo_list, titles)

def init_to_do_list(dvd_path, output_file=None):
    import sys
    titles = scan_for_dvds(dvd_dir, sys.stdout)
    if output_file is None:
        write_to_do_encode_list(titles)
    else:
        write_encode_list(output_file, titles)

def read_done_encode_list():
    return read_encode_list(options.encoded_list)

def write_done_encode_list(titles):
    return write_encode_list(options.encoded_list, titles)

def contained(title, titles):
    interesting_fields = 'dvd_path', 'title'

    for known_title in titles:
        if all([title[field] == known_title[field] for field in interesting_fields]):
            return True
    return False

def get_known_titles():
    return read_done_encode_list() + read_to_do_encode_list()

def remove_known_titles(titles):
    known_titles = get_known_titles()

    unknown_titles = []
    for title in titles:
        if not contained(title, known_titles):
            unknown_titles.append(title)
    return unknown_titles

def scan_for_dvds(dvd_dir, logfile):
    known_titles = get_known_titles()

    result = []
    for name, dvd_path, video_type in dvd_analysis.search_for_dvds(dvd_dir):
        if any(dvd_path == title['dvd_path'] for title in known_titles):
            continue

        if video_type == dvd_analysis.FILM:
            titles = dvd_analysis.get_main_feature(dvd_path, logfile)
        elif video_type == dvd_analysis.SERIES:
            titles = dvd_analysis.get_series_titles(dvd_path, logfile)
        elif video_type == dvd_analysis.MUSIC:
            titles = dvd_analysis.get_main_feature(dvd_path, logfile)
        else:
            titles = []

        for title, length in titles:
            result.append(dict(dvd_path=dvd_path, name=name, title=title, comment=time_utils.format_time(length)))

    return result

from contextlib import contextmanager

@contextmanager
def iterate_titles_to_encode(dvd_dir, logfile):
    titles = scan_for_dvds(dvd_dir, logfile)
    #titles = remove_known_titles(titles)
    titles = read_to_do_encode_list() + titles

    write_to_do_encode_list(titles[1:])
    encode_now = titles[0]

    try:
        yield encode_now['name'], encode_now['dvd_path'], encode_now['title']
    finally:
        titles = read_done_encode_list() + [encode_now]
        write_done_encode_list(titles)


if __name__ == '__main__':
    import sys
    args = sys.argv[1:]
    if len(args) == 0:
        dvd_dir = options.source_dvd_directory
        output_file = None
    elif len(args) == 1:
        dvd_dir = options.source_dvd_directory
        output_file = args[0]
    elif len(args) == 2:
        dvd_dir = args[0]
        output_file = args[1]
    init_to_do_list(dvd_dir, output_file)


