import os, shutil, setuptools

print 'Installing python packages'
setuptools.setup(
    name = "DVD Transcoder",
    version = "0.1.0",
    author = "Damien Ruscoe",
    author_email = "chuddah@gmail.com",
    description = ("Tool to automatically encode a directory of DVD images."),
    license = "BSD",
    keywords = "DVD Transcode",
    url = "http://chuddah.ddns.net",
    packages=['video_utils', 'coreutils'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
    scripts=['bin/analyze_dvd', 'bin/encode_dvds'],
)

print 'Copying config template file'
shutil.copy('config/dvd_encoder_options', os.path.expanduser('~/.dvd_encoder_options'))
