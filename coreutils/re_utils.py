import re

def iterate_matches(lines, pattern):
    for line in lines:
        m = re.match(pattern, line)
        if m :
            yield m.groupdict()
