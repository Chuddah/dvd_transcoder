import os

def read_file(filename):
    with open(filename, 'r') as f:
        return f.readlines()

def write_file(filename, lines):
    with open(filename, 'w+') as f:
        f.writelines('\n'.join(lines))

def listdir(directory, predicate):
    contents = os.listdir(directory)
    for item in contents:
        path = os.path.join(directory, item)
        if predicate(path):
            yield item

def directories(directory):
    return listdir(directory, os.path.isdir)

def files(directory):
    return listdir(directory, os.path.isfile)

def search_for_folders(search_directory, folder):
    folders = directories(search_directory)

    if folder in folders:
        yield os.path.join(search_directory, folder)

    for subdir in conents:
        subpath = os.path.join(search_directory, subdir)
        for result in search_for_folders(subpath, folder):
            yield result

def extension(filename):
    extension = os.path.splitext(filename)[1].lower()
    if extension.startswith('.'):
        return extension[1:]
    return extension

def replace_extension(filename, new_extension):
    name = os.path.splitext(filename)[0]
    return '%s.%s' % (name, new_extension)

def change_location(path, location):
    output_filename = os.path.split(path)[1]
    return os.path.join(location, output_filename)




class NullLogger(object):
    def write(*args, **kargs):
        pass

NullLogger.instance = NullLogger()

def remove_files(filenames, logfile=NullLogger.instance):
    for filename in filenames:
        logfile.write('  %s ... ' % filename)
        try:
            os.unlink(filename)
            logfile.write('Done\n')
        except Exception, e:
            logfile.write('Failed\n')

if __name__ == '__main__':
    print 'FOLDERS'
    print '\n'.join(directories(os.getcwd()))
    print
    print 'FILES'
    print '\n'.join(files(os.getcwd()))
