import subprocess

PS1 = '>>> '

class RunError(RuntimeError) :
    """A utility returned a failure exit status"""

def _execute(cmd):
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell = True)
    lines = [line.rstrip() for line in p.stdout]

    if p.wait() != 0 :
        raise RunError(cmd + " returned an error exit status, see \n" + "\n".join(lines))

    return lines

def execute(cmd, logfile=None):
    """ Executes a command in a subprocess """
    if isinstance(cmd, (list, tuple)):
        cmd = ' '.join(cmd)
    if logfile:
        logfile.write('%s%s\n' % (PS1, cmd))
    return _execute(cmd)

def quote(string_or_list):
    """ Quotes a string that may contain spaces for use in a CLI command """
    if isinstance(string_or_list, (tuple, list)):
        return [quote(item) for item in string_or_list]
    string_or_list.replace(' ', '\ ')
    return "\"%s\"" % string_or_list

if __name__ == '__main__':
    cwd = execute('pwd')[0]
    contents = '\n'.join(execute('ls'))
    print 'Contents of %s\n%s' % (cwd, contents)
