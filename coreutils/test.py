import sys
sys.path.append('src')
sys.path.append('../src')

import traceback

class Failure(Exception):
    """ Test failure exception """

class TestRunner(object):

    def __init__(self):
        self.failures = []
        self.errors = []
        self.passes = []
        self.output = sys.stdout

    def write(self, text='\n'):
        sys.stdout.write(str(text))

    def run(self, test_case):
        try:
            test_case()
        except Failure, e:
            tb = traceback.format_exc()
            if hasattr(e, 'tb'):
                tb = e.tb
            self.failures.append((test_case, e, tb))
            self.write('F')
        except Exception, e:
            self.errors.append((test_case, e, traceback.format_exc()))
            self.write('E')
        else:
            self.passes.append(test_case)
            self.write('.')

    def results(self):
        return dict(
            failures = len(self.failures), 
            errors = len(self.errors),
            passes = len(self.passes),
            total = len(self.passes) + len(self.errors) + len(self.failures),
        )

    def summary(self):
        self.write()
        self.write()
        if self.errors:
            self.write('ERRORS')
            self.write('-' * 80)
            for test_case, error, tb in self.errors:
                self.write(error)
                self.write(tb)
        if self.failures:
            self.write('-' * 80)
            self.write('FAILURES')
            self.write('-' * 80)
            for test_case, failure, tb in self.failures:
                self.write(test_case)
                self.write(failure)
                self.write(tb)
        self.write('-' * 80)
        self.write("\n  %(total)d tests executed\n  %(failures)d failed\n  %(errors)d errors" % self.results())
        self.write()
        




class TestCase(object):
    def __init__(self, test_fn, params=[], test_number=None):
        self.test_fn = test_fn
        self.params = params
        self.test_number = test_number

    def execute(self, test_runner):
        test_runner.run(self)

    def __call__(self):
        self.test_fn(*self.params)

    def __str__(self):
        str = self.test_fn.__name__
        if self.test_number is not None:
            str += '[%d]' % self.test_number
        str += '(%s)' % ', '.join([repr(p) for p in self.params])
        return str

class TestSuite(object):
    def __init__(self):
        self.test_cases = []

    def add_test_case(self, test_case):
        self.test_cases.append(test_case)

    def execute(self, test_runner):
        for test_case in self.test_cases:
            test_case.execute(test_runner)




__global_test_suite = TestSuite()

def test_function(test_fn):
    test_case = TestCase(test_fn)
    __global_test_suite.add_test_case(test_case)

def parameterized_test(test_fn, parameters_list):
    for n, params in enumerate(parameters_list):
        test_case = TestCase(test_fn, params=params, test_number=n)
        __global_test_suite.add_test_case(test_case)

def test(test_fn_or_params):
    if isinstance(test_fn_or_params, list):
        return lambda test_fn: parameterized_test(test_fn, test_fn_or_params)
    else:
        test_function(test_fn_or_params)


def fail(message):
    raise Failure(message)

def assertEquals(expected, value, mesg=""):
    if expected != value:
        fail('%s expected, but got %s' % (expected, value))

def assertTrue(result, mesg=""):
    assertEquals(True, result, mesg)

def assertFalse(result, mesg=""):
    assertEquals(False, result, mesg)

def assertRaises(ExceptionType, fn, *args, **kargs):
    try:
        result = fn(*args, **kargs)
    except ExceptionType, e:
        pass
    except Exception, e:
        exception = Failure('%s exception expected, but got %s exception' % (ExceptionType.__name__, e.__class__.__name__))
        exception.tb = traceback.format_exc()
        raise exception
    else:
        fail('%s exception expected, but function returned %s' % (ExceptionType.__name__, result))



"""
@test
def test_test_pass():
    return

@test
def test_test_fail():
    assertEquals('foo', 'bar')

@test
def test_test_error():
    None.doesnt_have_this_method()

"""

def main():
    test_runner = TestRunner()
    __global_test_suite.execute(test_runner)
    test_runner.summary()

test.main = main


if __name__ == '__main__':
    test.main()
