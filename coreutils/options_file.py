import re
from collections import namedtuple

from coreutils import file_utils

comment_parse = '(?P<interesting>.*?)(?P<comment>#.*)'
option_parse = '(?P<option>.*?)=(?P<value>.*)'

def parse_comments(contents):
    LineType = namedtuple('OptionsFileLine', 'contents comment')

    result = []
    comment_pattern = re.compile(comment_parse)

    for line in contents:
        comment_match = comment_pattern.match(line)
        if comment_match:
            matches = comment_match.groupdict()
            comment = matches['comment']
            interesting = matches['interesting']

            result.append(LineType(interesting, comment))
        else:
            result.append(LineType(line, ''))

    return result

def parse_simple_options(contents):
    option_pattern = re.compile(option_parse)

    result = {}
    for line, comment in parse_comments(contents):
        option_match = option_pattern.match(line)
        if option_match:
            matches = option_match.groupdict()
            option = matches['option'].strip()
            value = matches['value'].strip()
            result[option] = value
        elif line:
            option = line.strip()
            result[option] = ''
    return result

def parse_file(filename, parse_method):
    contents = file_utils.read_file(filename)
    return parse_method(contents)


