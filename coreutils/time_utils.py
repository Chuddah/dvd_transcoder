import time
import datetime

def div_mod(value, divisor):
    return value // divisor, value % divisor

def _split_time(time):
    """ Internal function to split a number of seconds into time components 
    >>> _split_time(0)
    (0, 0, 0, 0, 0)
    >>> _split_time(1)
    (0, 0, 0, 0, 1)
    >>> _split_time(60)
    (0, 0, 0, 1, 0)
    >>> _split_time(60 * 60)
    (0, 0, 1, 0, 0)
    >>> _split_time(24 * 60 * 60)
    (0, 1, 0, 0, 0)
    >>> _split_time(365 * 24 * 60 * 60)
    (1, 0, 0, 0, 0)
    """
    minutes, seconds = div_mod(time, 60)
    hours, minutes = div_mod(minutes, 60)
    days, hours = div_mod(hours, 24)
    years, days = div_mod(days, 365)
    return years, days, hours, minutes, seconds

def time_str():
    years, days, hours, minutes, seconds = _split_time(time.time())
    return '%02d:%02d:%02d' % (hours, minutes, seconds)

def format_time(time):
    """ Returns a formatted string of a time given in seconds
    >>> format_time(0)
    '00:00'
    >>> format_time(1)
    '00:01'
    >>> format_time(60)
    '01:00'
    >>> format_time(60 * 60)
    '1:00:00'
    """
    years, days, hours, minutes, seconds = _split_time(time)

    if years:
        return '%d years %d days %02d:%02d:%02d' % (years, days, hours, minutes, seconds)
    elif days:
        return '%d days %02d:%02d:%02d' % (days, hours, minutes, seconds)
    elif hours:
        return '%d:%02d:%02d' % (hours, minutes, seconds)
    else:
        return '%02d:%02d' % (minutes, seconds)

def parse_time(time):
    """ Parses a time string
    >>> parse_time('0:00')
    0
    >>> parse_time('0:01')
    1
    >>> parse_time('0:59')
    59
    >>> parse_time('1:00')
    60
    >>> parse_time('1:00:00')
    3600
    """
    components = time.split(':')
    weights = (365*24*60*60, 24*60*60, 60*60, 60, 1)

    seconds = 0
    for weight, value in zip(weights[-len(components):], components):
        seconds += weight * int(value)
    return seconds


def time_since(start):
    now = time.time()
    return format_time(int(now-start))

def is_weekday():
    MONDAY = 0
    FRIDAY = 4
    today = datetime.date.today()
    return MONDAY <= today.weekday() <= FRIDAY

def am_i_at_work():
    if is_weekday():
        years, days, hours, minutes, seconds = _split_time(time.time())
        return (06, 40) <= (hours, minutes) <= (17, 00)
    return False

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    print time_str()
